/*******************************************************************************
*         Copyright (c), NXP Semiconductors Gratkorn / Austria
*
*                     (C)NXP Semiconductors
*       All rights are reserved. Reproduction in whole or in part is
*      prohibited without the written consent of the copyright owner.
*  NXP reserves the right to make changes without notice at any time.
* NXP makes no warranty, expressed, implied or statutory, including but
* not limited to any implied warranty of merchantability or fitness for any
*particular purpose, or that the use will not infringe any third party patent,
* copyright or trademark. NXP must not be liable for any loss or damage
*                          arising from its use.
********************************************************************************
*
* Filename:          RegCtl_SpiHw.c
* Processor family:  LPC11xx
*
* Description:       This file holds the functions for the SPI communication.
*                    In case of the Silica TUSA board, the SPI communication
*                    gets emulated, because it uses GPIO pins that are not
*                    designated for use of SPI communication.
*******************************************************************************/
#include <ph_Status.h>
#include <RegCtl_SpiHw.h>
//#include "stm32f10x.h"
//#include "stm32f10x_spi.h"
#include "stm32l4xx_hal.h"

#if CONFIG_ENABLE_DRIVER_SSP==1
/* statistics of all the interrupts */
volatile uint32_t interruptRxStat0 = 0;
volatile uint32_t interruptOverRunStat0 = 0;
volatile uint32_t interruptRxTimeoutStat0 = 0;

volatile uint32_t interruptRxStat1 = 0;
volatile uint32_t interruptOverRunStat1 = 0;
volatile uint32_t interruptRxTimeoutStat1 = 0;
#endif

//端口定义--------------------------------------------------------------
//CS 
#define SPI_CS_GPIO		      GPIOB
#define SPI_CS_GPIO_PIN	    GPIO_PIN_12
#define SPI_CS_GPIO_H()		HAL_GPIO_WritePin(SPI_CS_GPIO, SPI_CS_GPIO_PIN,GPIO_PIN_SET);
#define SPI_CS_GPIO_L()		HAL_GPIO_WritePin(SPI_CS_GPIO, SPI_CS_GPIO_PIN,GPIO_PIN_RESET);

//MOSI
#define SPI_MOSI_GPIO		  GPIOB
#define SPI_MOSI_GPIO_PIN	GPIO_PIN_15
#define SPI_MOSI_GPIO_H()	HAL_GPIO_WritePin(SPI_MOSI_GPIO, SPI_MOSI_GPIO_PIN,GPIO_PIN_SET);
#define SPI_MOSI_GPIO_L()	HAL_GPIO_WritePin(SPI_MOSI_GPIO, SPI_MOSI_GPIO_PIN,GPIO_PIN_RESET);


//SCK
#define SPI_SCLK_GPIO		GPIOB
#define SPI_SCLK_GPIO_PIN	GPIO_PIN_13
#define SPI_SCLK_GPIO_H()	HAL_GPIO_WritePin(SPI_SCLK_GPIO, SPI_SCLK_GPIO_PIN,GPIO_PIN_SET);
#define SPI_SCLK_GPIO_L()	HAL_GPIO_WritePin(SPI_SCLK_GPIO, SPI_SCLK_GPIO_PIN,GPIO_PIN_RESET);
//MISO
#define SPI_MISO_GPIO		GPIOB
#define SPI_MISO_GPIO_PIN	GPIO_PIN_14



//RST
#define RF_RST_GPIO		      GPIOC
#define RF_RST_GPIO_PIN	    GPIO_PIN_6
#define RF_RST_GPIO_H()	    HAL_GPIO_WritePin(RF_RST_GPIO, RF_RST_GPIO_PIN,GPIO_PIN_SET);
#define RF_RST_GPIO_L()	    HAL_GPIO_WritePin(RF_RST_GPIO, RF_RST_GPIO_PIN,GPIO_PIN_RESET);




void SPI2_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;
	__HAL_RCC_GPIOB_CLK_ENABLE();
	__HAL_RCC_GPIOC_CLK_ENABLE();
	GPIO_InitStruct.Pin = SPI_SCLK_GPIO_PIN|SPI_MOSI_GPIO_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	//GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

//MISO	
	GPIO_InitStruct.Pin = SPI_MISO_GPIO_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;//既然是模拟 就从0开始//GPIO_Mode_IPU; 
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);
	
//CS
  GPIO_InitStruct.Pin = SPI_CS_GPIO_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

//RESET
  GPIO_InitStruct.Pin = RF_RST_GPIO_PIN;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	SPI_MOSI_GPIO_H();
	SPI_SCLK_GPIO_H();
}
//读头复位：复位脚为PA4
void Reset_RC663_device(void)
{
	uint32_t volatile i;


	/* RSET signal low */
	RF_RST_GPIO_L();

	/* delay of ~1,2 ms */
	for (i = 0x2000; i > 0; i --);

	/* RSET signal high to reset the RC663 IC */
	RF_RST_GPIO_H();

	/* delay of ~1,2 ms */
	for (i = 0x2000; i > 0; i --);

	/* RSET signal low */
	RF_RST_GPIO_L();

	/* delay of ~1,2 ms */
	for (i = 0x2000; i > 0; i --);
}

/*********************************************************************/
/*
 * @brief 		Initialise the spi SSP0 for master mode
 * @param[in]	None
 *
 * @return 		None
 **********************************************************************/
void RegCtl_SpiHwInit(void)
{
	SPI2_Init();			/* Select the SSP0 for SPI interface */
}
/*****************************************************************************
** Function name:		_SSP0_SetReg
**
** Descriptions:		Send 2 databytes [RegNr, RegVal] to the SSP0 port
**
** parameters:			buffer pointer
** Returned value:		None
**
*****************************************************************************/
void _SSP0_SetReg(uint8_t *buf)
{
	uint8_t address,value,i;

	address = *buf++;	//地址
	value = *buf;		//数据

	SPI_SCLK_GPIO_L();
	SPI_CS_GPIO_L();
	
	for(i=8;i>0;i--)
    {
		if(address&0x80)
		{
		  SPI_MOSI_GPIO_H();
		}
		else
		{
		  SPI_MOSI_GPIO_L();
		}
	    SPI_SCLK_GPIO_H();
        address <<= 1;
		SPI_SCLK_GPIO_L();
    }
	
	for(i=8;i>0;i--)
    {
		if(value&0x80)
		{
		  SPI_MOSI_GPIO_H();
		}
		else
		{
		  SPI_MOSI_GPIO_L();
		}
	    SPI_SCLK_GPIO_H();
        value <<= 1;
	    SPI_SCLK_GPIO_L();
    }

   	SPI_CS_GPIO_H();
   	SPI_SCLK_GPIO_H();
	
}

void _SSP0_GetReg(uint8_t *buf)
{
	 uint8_t i, ucAddr;
     uint8_t ucResult=0;
	 
	 SPI_SCLK_GPIO_L();
	 SPI_CS_GPIO_L();
	 
	 ucAddr = *buf++;	//地址

     for(i=8;i>0;i--)
     {
		 if(ucAddr&0x80)
		 {
		   SPI_MOSI_GPIO_H();
		 }
		 else
		 {
		   SPI_MOSI_GPIO_L();
		 }
		 SPI_SCLK_GPIO_H();
         ucAddr <<= 1;
		 SPI_SCLK_GPIO_L();
     }

     for(i=8;i>0;i--)  //接收数据
     {
	     SPI_SCLK_GPIO_H();
         ucResult <<= 1;

		 if (HAL_GPIO_ReadPin(SPI_MISO_GPIO,SPI_MISO_GPIO_PIN))//HAL
		 {
		   ucResult|=1;
		 }
		 SPI_SCLK_GPIO_L();
     }

	 *buf = ucResult;
	  
	 SPI_CS_GPIO_H();
	 SPI_SCLK_GPIO_H();
}
/*********************************************************************/
/*
 * @brief 		Get a databyte from a register
 * @param[in]	address		Address of the register
 * @param[in]	*reg_data	Pointer to the databyte to be read
 *
 * @return 		- PH_ERR_BFL_SUCCESS
 *
 **********************************************************************/
phStatus_t RegCtl_SpiHwGetReg(uint8_t address, uint8_t *reg_data)
{
	uint8_t buff[2];

	/* load address in tx_buff */
	buff[0] = address;

	/* Assert the SSEL pin */
	SPI_CS_GPIO_L();

	/* receive 2 bytes: dummy byte + valid byte */
	_SSP0_GetReg(buff);

	/* Deassert the SSEL pin */
	SPI_CS_GPIO_H();

	/* 2nd valid databyte */
	reg_data[1] = buff[1];

	/* return success */
	return PH_ERR_SUCCESS;
}

/*********************************************************************//**
 * @brief 		Set a databyte to a register
 * @param[in]	address		Address of the register
 * @param[in]	*reg_data	Databyte to be written
 *
 * @return 		- PH_ERR_BFL_SUCCESS
 *
 **********************************************************************/
phStatus_t RegCtl_SpiHwSetReg(uint8_t address, uint8_t reg_data)
{
	uint8_t buff[2];

	/* load address and data in buffer */
	buff[0] = address;
	buff[1] = reg_data;

	/* Assert the SSEL pin */
	SPI_CS_GPIO_L();

	/* send SPI frame */
	_SSP0_SetReg(buff);

	/* Deassert the SSEL pin */
	SPI_CS_GPIO_H();

	/* return success */
	return PH_ERR_SUCCESS;
}

#if 0
/*********************************************************************//**
 * @brief 		Modify a bit in a register
 * @param[in]	address		Address of the register
 * @param[in]	*mask		Mask of the bit to set/reset
 * @param[in]	set			1: set 1: reset
 *
 * @return 		- PH_ERR_BFL_SUCCESS
 *
 **********************************************************************/
phStatus_t RegCtl_SpiHwModReg(uint8_t address, uint8_t mask, uint8_t set)
{
    uint8_t  reg_data[2];

    /* get the register first */
    RegCtl_SpiHwGetReg(address, reg_data);

	if (set)
	{
		/* The bits of the mask, set to one are set in the new data: */
		reg_data[1] |= mask;
	}
	else
	{
		/* The bits of the mask, set to one are cleared in the new data: */
		reg_data[1] &= (uint8_t)(~mask);
	}

	/* set the register */
	RegCtl_SpiHwSetReg(address, reg_data[1]);

	/* return success */
	return PH_ERR_SUCCESS;
}
/*********************************************************************/
/*
 * @brief 		Get a databyte from a register
 * @param[in]	address		Address of the register
 * @param[in]	*reg_data	Pointer to the databyte to be read
 *
 * @return 		- PH_ERR_BFL_SUCCESS
 *
 **********************************************************************/
phStatus_t RegCtl_SpiHwSetMultiData(uint8_t *buf, uint32_t Len)
{
	/* Assert the SSEL pin */
    RC663_CS_CLR();
	_SSP0_SetMultiData(buf, Len);

	/* Deassert the SSEL pin */
	RC663_CS_SET();

	/* return success */
	return PH_ERR_SUCCESS;
}
/*********************************************************************/
/*
 * @brief 		Get a databyte from a register
 * @param[in]	address		Address of the register
 * @param[in]	*reg_data	Pointer to the databyte to be read
 *
 * @return 		- PH_ERR_BFL_SUCCESS
 *
 **********************************************************************/
phStatus_t RegCtl_SpiHwGetMultiData(uint8_t *txbuf, uint8_t *rxbuf, uint32_t Len)
{
	/* Assert the SSEL pin */
	RC663_CS_CLR();
	_SSP0_GetMultiData(txbuf, rxbuf, Len);

	/* Deassert the SSEL pin */
	RC663_CS_SET();
	/* return success */
	return PH_ERR_SUCCESS;
}
#endif
/******************************************************************************
**                            End Of File
******************************************************************************/


