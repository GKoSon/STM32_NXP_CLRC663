#include "GUSART.h"
#include "stm32l4xx_hal.h"//UART_HandleTypeDef
#include <string.h>//memcpy
uint8_t DO08(mqttDataStrType *pMsg){printf("DO08\n"); return 0 ; }
uint8_t DO09(mqttDataStrType *pMsg){printf("DO09\n");return 0 ;}
typedef struct _AppHandleArry
{
    uint8_t Id;
    uint8_t (*EventHandlerFn)(mqttDataStrType *pMsg);
}AppHandleArryType;

static AppHandleArryType gAppTaskArry[]=
{
  {0x09 , DO09},
  {0x08 , DO08},
  
};
void Usart1_Server(USART_RECEIVETYPE *UsartType)
{
	char i=0;
	mqttDataStrType Udata={0};
	memcpy((uint8_t*)&Udata,UsartType->usartDMA_rxBuf,sizeof(mqttDataStrType));
  printf("ID is %d\n",Udata.msgType);
	for(i=0;i<sizeof(gAppTaskArry)/sizeof(gAppTaskArry[0]);i++)//个数相除才是数量
		if(Udata.msgType==gAppTaskArry[i].Id)
			gAppTaskArry[i].EventHandlerFn(&Udata);
		
	
}






