#ifndef _GUSART_H_
#define _GUSART_H_

#include "stdint.h"//uint8_t ��
#include "usart.h"//huart1 USART_RECEIVETYPE


typedef struct
{
	uint8_t  msgType;
	uint16_t sqeId;
	uint16_t serType;
	uint16_t cmd;
	uint8_t data[15];
	uint16_t len;
}mqttDataStrType;
extern void Usart1_Server(USART_RECEIVETYPE *UsartType);
#endif
