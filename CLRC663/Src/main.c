
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "dma.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "GUSART.h"
#include "public.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
#include <RegCtl_SpiHw.h>
#include "bsp_spiflash.h"

//读UID号码
#define CHIP_ID_START        0x1FFF7590
//读内存大小
#define CHIP_FLASHSIZE_START 0x1FFF75E0
void Get_ChipID(unsigned char *p)
{
    unsigned char i;
    unsigned char *pIDStart=(unsigned char *)(CHIP_ID_START);       
    for(i=0;i!=12;i++)
    {
        *p++=*pIDStart++;
    }
}
unsigned short Get_ChipSize(unsigned short *p)
{
		static unsigned short size;
    size =*(unsigned short *)(CHIP_FLASHSIZE_START);//直接读出 没必要每个U8在拼接       
		*p = size;
	  //p[0] = size;//可以的
	  //p = &size;//不可以的
	  return size;
}
void STM32_Info_test(void)
{
    uint8_t  ChipId[12] ;
	  unsigned short size = 0;
	
		Get_ChipSize(&size);
	  printf("1#%dM\n",size);
		size =Get_ChipSize(&size);
		printf("2#%dM\n",size);
   // config.read(CFG_SYS_CHIP_ID , (void **)&ChipId);
    Get_ChipID(ChipId);
	  for(int i=0;i<12;i++)
	     printf("%02x-",ChipId[i]);
	  printf("\n");
	// printf("%.12s\n",ChipId);


}
void IO_Test(void)
{
	uint8_t i=0;	
	HAL_Delay(100);
	HAL_GPIO_WritePin(BEEP_GPIO_Port, BEEP_Pin,GPIO_PIN_SET);//叫
	HAL_Delay(100);
	HAL_GPIO_WritePin(BEEP_GPIO_Port, BEEP_Pin,GPIO_PIN_RESET);//不叫
	HAL_Delay(100);
	HAL_GPIO_WritePin(RELAY_GPIO_Port, RELAY_Pin,GPIO_PIN_RESET);
	HAL_Delay(100);
	HAL_GPIO_WritePin(RELAY_GPIO_Port, RELAY_Pin,GPIO_PIN_SET);
	HAL_Delay(100);
	HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin,GPIO_PIN_RESET);//不亮
	HAL_Delay(100);
	HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin,GPIO_PIN_SET);//亮
	HAL_Delay(100);
		
	HAL_UART_Transmit(&huart1, &i,  1, 0xFFFF);//发送u8数组

}

void KEY_Test(void)
{
	uint8_t status=HAL_GPIO_ReadPin(KEY_GPIO_Port,KEY_Pin);//没有按下是1 按下就是0	
		if(status)
		{
		
			HAL_GPIO_WritePin(BEEP_GPIO_Port, BEEP_Pin,GPIO_PIN_RESET);//不叫
		}
		else
		{
			HAL_GPIO_WritePin(BEEP_GPIO_Port, BEEP_Pin,GPIO_PIN_SET);//叫
		}
}

void Show_Card_Activate(void)
{
	printf("有卡啦\n\n");
	HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin,GPIO_PIN_SET);//亮
	HAL_GPIO_WritePin(BEEP_GPIO_Port, BEEP_Pin,GPIO_PIN_SET);//叫
	HAL_Delay(100);
	HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin,GPIO_PIN_RESET);//不亮			
	HAL_GPIO_WritePin(BEEP_GPIO_Port, BEEP_Pin,GPIO_PIN_RESET);//不叫
}


unsigned short flash=0;
uint8_t bHalBufferReader[0x40];
uint8_t bBufferReader[0x60];
uint8_t Rout[16];
/* USER CODE END 0 */
uint8_t pAtqa[2]={0};
uint8_t pAts[255] = {0}; 

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
	static uint8_t Key[6] = {0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU};
	char i =0 ;
	phbalReg_Stub_DataParams_t balReader;
	phStatus_t status;
  phhalHw_Rc663_DataParams_t halReader;
	void *pHal;


	phpalI14443p3a_Sw_DataParams_t I14443p3a;
	phpalI14443p4_Sw_DataParams_t I14443p4;//bug
	phpalI14443p4a_Sw_DataParams_t I14443p4a;
	phpalMifare_Sw_DataParams_t palMifare;
	phKeyStore_Rc663_DataParams_t Rc663keyStore;
	phalMfc_Sw_DataParams_t alMfc;
	
	
	uint8_t bSak[1];
	uint8_t bUid[10];
	uint8_t bMoreCardsAvailable;
	uint8_t bLength;	
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART1_UART_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */
HAL_UART_Receive_DMA(&huart1, UsartType1.usartDMA_rxBuf, RECEIVELEN);
__HAL_UART_ENABLE_IT(&huart1, UART_IT_IDLE);
//STM32_Info_test();
//W24Q64_Test();  

	Reset_RC663_device();
	phbalReg_Stub_Init(&balReader, sizeof(phbalReg_Stub_DataParams_t));	
	status = phhalHw_Rc663_Init(&halReader,sizeof(phhalHw_Rc663_DataParams_t),&balReader,0,bHalBufferReader,sizeof(bHalBufferReader),bHalBufferReader,sizeof(bHalBufferReader));
 
	halReader.bBalConnectionType = PHHAL_HW_BAL_CONNECTION_SPI;
	pHal = &halReader;
		/* Initializing specific objects for the communication with
	 * Mifare (R) Classic cards.
	 * The Mifare (R) Classic card is compliant of
	 * ISO 14443-3 and ISO 14443-4
	*/
	  /*在RC663/phsubBal.h 最后是TOOL 用了CRC Initialize the 14443-3A PAL (Protocol Abstraction Layer) component */
	PH_CHECK_SUCCESS_FCT(status, phpalI14443p3a_Sw_Init(&I14443p3a,sizeof(phpalI14443p3a_Sw_DataParams_t), pHal));
	  /* Initialize the 14443-4 PAL component */
	PH_CHECK_SUCCESS_FCT(status, phpalI14443p4_Sw_Init(&I14443p4,sizeof(phpalI14443p4_Sw_DataParams_t), pHal));
	  /* Initialize the Mifare PAL component */
	PH_CHECK_SUCCESS_FCT(status, phpalMifare_Sw_Init(&palMifare,sizeof(phpalMifare_Sw_DataParams_t), pHal, &I14443p4));
	  /* Initialize the keystore component */
	PH_CHECK_SUCCESS_FCT(status, phKeyStore_Rc663_Init(&Rc663keyStore,sizeof(phKeyStore_Rc663_DataParams_t), pHal));
	  /* Initialize the Mifare (R) Classic AL component - set NULL because
	   * the keys are loaded in E2 by the function */
	/* phKeyStore_SetKey */
	PH_CHECK_SUCCESS_FCT(status, phalMfc_Sw_Init(&alMfc,sizeof(phalMfc_Sw_DataParams_t), &palMifare,NULL));
	/* SoftReset the IC.The SoftReset only resets the RC663 to EEPROM configuration. */
	PH_CHECK_SUCCESS_FCT(status, phhalHw_Rc663_Cmd_SoftReset(pHal));
	/* Read the version of the reader IC */
	PH_CHECK_SUCCESS_FCT(status, phhalHw_ReadRegister(&halReader,PHHAL_HW_RC663_REG_VERSION, bBufferReader));
	/* Reset the Rf field */
	PH_CHECK_SUCCESS_FCT(status, phhalHw_FieldReset(pHal));
	/* Apply the type A protocol settings and activate the RF field. */
	PH_CHECK_SUCCESS_FCT(status, phhalHw_ApplyProtocolSettings(pHal,PHHAL_HW_CARDTYPE_ISO14443A));
	/* Activate the communication layer part 3 of the ISO 14443A standard. */
//  UsartType1.receive_flag=0;	
//    phhalHw_Rc663_SetConfig(pHal,PHHAL_HW_CONFIG_MODINDEX,0X01);//貌似没有用
/* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {


  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
		
	 status = phpalI14443p3a_HaltA (&I14443p3a);  //挂起卡片 大有用途！！
	 status = phpalI14443p3a_ActivateCard(&I14443p3a, NULL, 0x00, bUid, &bLength, bSak, &bMoreCardsAvailable);
		/* Check if we have a card in the RF field.
		 * If so, check what card it is. */
		if (PH_ERR_SUCCESS == status)
		{
		Show_Card_Activate();
			/* Check if there is an ISO-4 compliant in the RF field */
		if (0x20 == *bSak)
			printf("CPU 卡\n");
		else if (0x28 == *bSak)
			printf("手环\n");
		else if (0x08 == *bSak)
			printf("M1 卡\n");
		else 
			printf("Not Know\n");
		
		for(i=0;i<bLength;i++)
			printf("%02x ",bUid[i]);
		printf("\n\n");
	
		//if(0x28 == *bSak)
		if(1)
		{
			printf("开始手环流程\n\n");		
			status = phpalI14443p3a_GetSerialNo(&I14443p3a, bUid,&bLength) ;//获得UID 雷同上面的
			if(PH_ERR_SUCCESS == status)
			{
			for(i=0;i<bLength;i++)
				printf("%02x ",bUid[i]);
			printf("\n\n");
			}
			else
				printf("886\n");
			
			
			

////可以挂起			
//			status = phpalI14443p3a_HaltA (&I14443p3a);  //挂起卡片 大有用途！！
//			if(PH_ERR_SUCCESS == status)
//					printf("挂起\n\n");
//			else
//					printf("没卵用\n");
//			
//			 HAL_Delay(200);

////没用 不如下面的		搭配挂起可以回来	
//			status = phpalI14443p3a_RequestA(&I14443p3a,pAtqa) ;//
//				if(PH_ERR_SUCCESS == status)
//				  printf("phpalI14443p3a_RequestA成功--%d--%d\n\n",pAtqa[0],pAtqa[1]);
//			else
//					printf("phpalI14443p3a_RequestA没卵用--%d--%d\n\n",pAtqa[0],pAtqa[1]);
//			
//				status = phpalI14443p3a_WakeUpA (&I14443p3a,pAtqa) ;//
//				if(PH_ERR_SUCCESS == status)
//				  printf("phpalI14443p3a_WakeUpA 成功--%d--%d\n\n",pAtqa[0],pAtqa[1]);
//			  else
//					printf("phpalI14443p3a_WakeUpA 卵用--%d--%d\n\n",pAtqa[0],pAtqa[1]);
//			
			
//再次唤醒为什么失败？	
//			status = phpalI14443p3a_ActivateCard(&I14443p3a, NULL, 0x00, bUid, &bLength, bSak, &bMoreCardsAvailable);
//				if(PH_ERR_SUCCESS == status)
//				  printf("\n\nphpalI14443p3a_ActivateCard成功\n\n");
//			 else
//					printf("\n\nphpalI14443p3a_ActivateCard失败\n\n");


//没用的			 
//			 status = phhalHw_SetConfig(&I14443p4, 0x005E, 0x0001);
//				if(PH_ERR_SUCCESS == status)
//				  printf("\n\nphhalHw_SetConfig成功\n\n");
//			 else
//					printf("\n\nphhalHw_SetConfig没卵用\n\n");
			
			
			
//			phpalI14443p4a_ActivateCard(
//            pDataParams->pPal1443p4aDataParams,
//            pDataParams->sTypeATargetInfo.sTypeA_I3P4.bFsdi,
//            pDataParams->sTypeATargetInfo.sTypeA_I3P4.bCid,
//            pDataParams->sTypeATargetInfo.sTypeA_I3P4.bDri,
//            pDataParams->sTypeATargetInfo.sTypeA_I3P4.bDsi,
//            pDataParams->sTypeATargetInfo.sTypeA_I3P4.pAts));

status = phhalHw_SetConfig(pHal, 0X005E, 1);//协议无法切换 我的LIB库太老了 没有这个项目 结构体不一样
if(PH_ERR_SUCCESS == status)
	printf("\n\n切换成功\n\n");
else
	printf("\n\n切换失败\n\n");

			 status = phpalI14443p4a_ActivateCard(&I14443p4a,0,0,0,0,pAts);//#include <phpalI14443p4a.h>
						if(PH_ERR_SUCCESS == status)
				  printf("\n\nphpalI14443p4a_ActivateCard成功\n\n");
			else
					printf("\n\nphpalI14443p4a_ActivateCard失败\n\n");
		}
		
		
		
		
		
		
		
		
		if(0x08 == *bSak)
		{
			printf("开始M1流程\n\n");
			status = phpalI14443p3a_GetSerialNo(&I14443p3a, bUid,&bLength) ;//获得UID 雷同上面的
			if(PH_ERR_SUCCESS == status)
			{
			for(i=0;i<bLength;i++)
				printf("%02x ",bUid[i]);
			printf("\n\n");
			}
			else
				printf("886\n");
 
		

			status = phpalMifare_MfcAuthenticate(&palMifare,0, PHPAL_MIFARE_KEYA,Key,bUid);
			if(PH_ERR_SUCCESS == status)
					printf("认证成功\n\n");
			else
					printf("认证失败\n");	
		

		 status = phalMfc_Authenticate(&alMfc,0,PHPAL_MIFARE_KEYA,0,0,bUid,bLength);
		 if(PH_ERR_SUCCESS == status)
				printf("认证成功!\n\n");
	   else
			  printf("认证失败!\n");	

		 
		 status = phalMfc_Read(&alMfc,0,Rout);
		  if(PH_ERR_SUCCESS == status)
			{
			 for(i=0;i<16;i++)
			 printf("%02x ",Rout[i]);
		   printf("\n\n");
			}
		  else
			  printf("读取失败!\n");	
			
			
			status = phalMfc_Write(&alMfc,2,Rout);	
			 if(PH_ERR_SUCCESS == status)
		    printf("写入成功 0块和2块内容一样了\n\n");
		   else
			  printf("写入失败!\n");	
			 
				 
			status = phpalI14443p3a_HaltA  (&I14443p3a);  //挂起卡片 大有用途！！
			if(PH_ERR_SUCCESS == status)
					printf("挂起\n\n");
			else
					printf("没卵用\n");	
		

	



		}
		

	 }

	// printf("status:%d\n",status);
	// HAL_Delay(100);
//		if(UsartType1.receive_flag)//如果产生了空闲中断
//		{
//			UsartType1.receive_flag=0;//清零标记
//			Usart1_Server(&UsartType1);
//		}
//		

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSE;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Enables the Clock Security System 
    */
  HAL_RCC_EnableCSS();

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
